package com.example.edwin.sharedpreferenceskotlin

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    internal lateinit var savedMessage: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        savedMessage = findViewById(R.id.saved_message)
    }

    fun save (view: View){
        val sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        val editor = sharedPreferences.edit();
        editor.putString("username",editUsername.text.toString());
        editor.putString("password",editPassword.text.toString());
        editor.apply();
        Toast.makeText(this,"Saving...",Toast.LENGTH_LONG).show();
    }

    fun show(view: View){
        val sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        val name = sharedPreferences.getString("username","Empty");
        val psw = sharedPreferences.getString("password","Empty");

        savedMessage.text = "Name: "+name+" "+"Password:"+psw;
    }
}
